"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiRedditRtSeenPostOptions = exports.ApiRedditRtSeenPostAttributes = exports.ApiRedditRtSeenPost = void 0;
const sequelize_1 = require("sequelize");
class ApiRedditRtSeenPost extends sequelize_1.Model {
}
exports.ApiRedditRtSeenPost = ApiRedditRtSeenPost;
exports.ApiRedditRtSeenPostAttributes = {
    id: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
        primaryKey: true,
        unique: true
    },
    firstSeen: {
        type: sequelize_1.DataTypes.DATE,
        allowNull: false,
        defaultValue: () => new Date()
    }
};
function ApiRedditRtSeenPostOptions(sequelize) {
    return {
        sequelize,
        tableName: "api_reddit_rt_SeenPost"
    };
}
exports.ApiRedditRtSeenPostOptions = ApiRedditRtSeenPostOptions;
