import { Sequelize, Model, ModelAttributes, InitOptions } from "sequelize";
export declare class ApiRedditRtSeenPost extends Model {
    id: string;
    firstSeen: Date;
}
export declare const ApiRedditRtSeenPostAttributes: ModelAttributes;
export declare function ApiRedditRtSeenPostOptions(sequelize: Sequelize): InitOptions;
