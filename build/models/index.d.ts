import GlobalSequelize from "@pcmrbotjs/global-sequelize";
import { Model } from "sequelize";
import { ApiRedditRtSeenPost } from "./reddit/rt/seenPost";
export default function initDb(connectionString: string): GlobalSequelize;
export interface ModelsInterface {
    ApiRedditRtSeenPost?: typeof ApiRedditRtSeenPost;
    [key: string]: typeof Model;
}
export { ApiRedditRtSeenPost };
