"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiRedditRtSeenPost = void 0;
const tslib_1 = require("tslib");
const global_sequelize_1 = tslib_1.__importDefault(require("@pcmrbotjs/global-sequelize"));
const seenPost_1 = require("./reddit/rt/seenPost");
Object.defineProperty(exports, "ApiRedditRtSeenPost", { enumerable: true, get: function () { return seenPost_1.ApiRedditRtSeenPost; } });
function initDb(connectionString) {
    const gs = new global_sequelize_1.default(connectionString);
    const registerRedditRtSeenPost = gs.models.ApiRedditRtSeenPost === undefined;
    // Initialize accounts
    if (registerRedditRtSeenPost) {
        seenPost_1.ApiRedditRtSeenPost.init(seenPost_1.ApiRedditRtSeenPostAttributes, seenPost_1.ApiRedditRtSeenPostOptions(gs.db));
        gs.models.ApiRedditRtSeenPost = seenPost_1.ApiRedditRtSeenPost;
    }
    // Establish associations
    // Synchronize models
    if (registerRedditRtSeenPost) {
        gs.models.ApiRedditRtSeenPost.sync({ alter: true });
    }
    return gs;
}
exports.default = initDb;
