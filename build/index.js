"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SlackUtilitiesService = exports.SlackWebService = exports.SlackCoreService = exports.RedditRtPostService = exports.RedditWikiService = exports.RedditUserService = exports.RedditSubmissionService = exports.RedditSubredditService = exports.RedditToolboxService = exports.RedditPostService = exports.RedditCoreService = exports.RedditCommentService = exports.ApiCoreService = void 0;
const tslib_1 = require("tslib");
// Core
const core_1 = tslib_1.__importDefault(require("./services/core"));
exports.ApiCoreService = core_1.default;
// Reddit
const comment_1 = tslib_1.__importDefault(require("./services/reddit/comment"));
exports.RedditCommentService = comment_1.default;
const core_2 = tslib_1.__importDefault(require("./services/reddit/core"));
exports.RedditCoreService = core_2.default;
const RedditPostService = require("./services/reddit/post");
exports.RedditPostService = RedditPostService;
const toolbox_1 = tslib_1.__importDefault(require("./services/reddit/toolbox"));
exports.RedditToolboxService = toolbox_1.default;
const subreddit_1 = tslib_1.__importDefault(require("./services/reddit/subreddit"));
exports.RedditSubredditService = subreddit_1.default;
const submission_1 = tslib_1.__importDefault(require("./services/reddit/submission"));
exports.RedditSubmissionService = submission_1.default;
const user_1 = tslib_1.__importDefault(require("./services/reddit/user"));
exports.RedditUserService = user_1.default;
const wiki_1 = tslib_1.__importDefault(require("./services/reddit/wiki"));
exports.RedditWikiService = wiki_1.default;
// Reddit RT
const post_1 = tslib_1.__importDefault(require("./services/reddit/rt/post"));
exports.RedditRtPostService = post_1.default;
// Slack 
const core_3 = tslib_1.__importDefault(require("./services/slack/core"));
exports.SlackCoreService = core_3.default;
const web_1 = tslib_1.__importDefault(require("./services/slack/web"));
exports.SlackWebService = web_1.default;
const utilities_1 = tslib_1.__importDefault(require("./services/slack/utilities"));
exports.SlackUtilitiesService = utilities_1.default;
function registerAllApiServices(broker) {
    broker.createService(core_1.default);
    broker.createService(comment_1.default);
    broker.createService(core_2.default);
    broker.createService(RedditPostService);
    broker.createService(toolbox_1.default);
    broker.createService(subreddit_1.default);
    broker.createService(submission_1.default);
    broker.createService(user_1.default);
    broker.createService(wiki_1.default);
    broker.createService(post_1.default);
    broker.createService(core_3.default);
    broker.createService(web_1.default);
    broker.createService(utilities_1.default);
}
exports.default = registerAllApiServices;
