"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const moleculer_1 = require("moleculer");
const request_promise_native_1 = tslib_1.__importDefault(require("request-promise-native"));
class ApiCoreService extends moleculer_1.Service {
    constructor(broker) {
        super(broker);
        this.parseServiceSchema({
            name: "api.core",
            version: 4,
            actions: {
                request: {
                    name: "request",
                    params: {
                        uri: {
                            type: "string",
                            optional: true,
                            convert: true
                        },
                        url: {
                            type: "string",
                            optional: true,
                            convert: true
                        },
                        baseUrl: {
                            type: "string",
                            convert: true,
                            optional: true
                        },
                        jar: {
                            type: "boolean",
                            convert: true,
                            optional: true
                        },
                        formData: {
                            type: "object",
                            optional: true
                        },
                        form: [
                            {
                                type: "object",
                                optional: true
                            },
                            {
                                type: "string",
                                convert: true,
                                optional: true
                            }
                        ],
                        auth: {
                            type: "object",
                            optional: true,
                            props: {
                                user: {
                                    type: "string",
                                    convert: true,
                                    optional: true
                                },
                                username: {
                                    type: "string",
                                    convert: true,
                                    optional: true
                                },
                                pass: {
                                    type: "string",
                                    convert: true,
                                    optional: true
                                },
                                password: {
                                    type: "string",
                                    convert: true,
                                    optional: true
                                },
                                sendImmediately: {
                                    type: "boolean",
                                    convert: true,
                                    optional: true
                                },
                                bearer: {
                                    type: "string",
                                    convert: true,
                                    optional: true
                                }
                            }
                        },
                        oauth: {
                            type: "object",
                            optional: true,
                            props: {
                                callback: {
                                    type: "string",
                                    convert: true,
                                    optional: true
                                },
                                consumer_key: {
                                    type: "string",
                                    convert: true,
                                    optional: true
                                },
                                consumer_secret: {
                                    type: "string",
                                    convert: true,
                                    optional: true
                                },
                                token: {
                                    type: "string",
                                    convert: true,
                                    optional: true
                                },
                                token_secret: {
                                    type: "string",
                                    convert: true,
                                    optional: true
                                },
                                transport_method: {
                                    type: "enum",
                                    optional: true,
                                    values: ["body", "header", "query"]
                                },
                                verifier: {
                                    type: "string",
                                    convert: true,
                                    optional: true
                                },
                                body_hash: [
                                    {
                                        type: "string",
                                        convert: true,
                                        optional: true
                                    },
                                    {
                                        type: "boolean",
                                        convert: true,
                                        optional: true
                                    }
                                ]
                            }
                        },
                        qs: {
                            type: "any",
                            optional: true
                        },
                        qsStringifyOptions: {
                            type: "any",
                            optional: true
                        },
                        qsParseOptions: {
                            type: "any",
                            optional: true
                        },
                        json: {
                            type: "any",
                            optional: true
                        },
                        forever: {
                            type: "any",
                            optional: true
                        },
                        host: {
                            type: "string",
                            convert: true,
                            optional: true
                        },
                        port: {
                            type: "number",
                            convert: true,
                            optional: true
                        },
                        method: {
                            type: "string",
                            convert: true,
                            optional: true
                        },
                        headers: {
                            type: "object",
                            optional: true
                        },
                        body: {
                            type: "any",
                            optional: true
                        },
                        family: {
                            type: "enum",
                            optional: true,
                            values: [4, 6]
                        },
                        followRedirect: {
                            type: "boolean",
                            convert: true,
                            optional: true
                        },
                        followAllRedirects: {
                            type: "boolean",
                            convert: true,
                            optional: true
                        },
                        followOriginalHttpMethod: {
                            type: "boolean",
                            convert: true,
                            optional: true
                        },
                        maxRedirects: {
                            type: "number",
                            convert: true,
                            optional: true
                        },
                        removeRefererHeader: {
                            type: "boolean",
                            convert: true,
                            optional: true
                        },
                        encoding: {
                            type: "any",
                            optional: true
                        },
                        timeout: {
                            type: "number",
                            convert: true,
                            optional: true
                        },
                        localAddress: {
                            type: "string",
                            convert: true,
                            optional: true
                        },
                        proxy: {
                            type: "any",
                            optional: true
                        },
                        tunnel: {
                            type: "boolean",
                            convert: true,
                            optional: true
                        },
                        strictSSL: {
                            type: "boolean",
                            convert: true,
                            optional: true
                        },
                        rejectUnauthorized: {
                            type: "boolean",
                            convert: true,
                            optional: true
                        },
                        time: {
                            type: "boolean",
                            convert: true,
                            optional: true
                        },
                        gzip: {
                            type: "boolean",
                            convert: true,
                            optional: true
                        },
                        preambleCRLF: {
                            type: "boolean",
                            convert: true,
                            optional: true
                        },
                        postambleCRLF: {
                            type: "boolean",
                            convert: true,
                            optional: true
                        },
                        withCredentials: {
                            type: "boolean",
                            convert: true,
                            optional: true
                        },
                        passphrase: {
                            type: "string",
                            convert: true,
                            optional: true
                        },
                        ca: [
                            {
                                type: "string",
                                convert: true,
                                optional: true
                            },
                            {
                                type: "array",
                                optional: true,
                                items: {
                                    type: "string",
                                    convert: true
                                }
                            }
                        ],
                        useQueryString: {
                            type: "boolean",
                            convert: true,
                            optional: true
                        },
                        simple: {
                            type: "boolean",
                            convert: true,
                            optional: true
                        },
                        resolveWithFullResponse: {
                            type: "boolean",
                            convert: true,
                            optional: true
                        }
                    },
                    handler: this.request
                }
            }
        });
    }
    async request(ctx) {
        return request_promise_native_1.default(ctx.params);
    }
}
exports.default = ApiCoreService;
