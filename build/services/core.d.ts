import { Service, Context } from "moleculer";
import rp from "request-promise-native";
export default class ApiCoreService extends Service {
    constructor(broker: any);
    request(ctx: Context<rp.Options, any>): Promise<rp.FullResponse>;
}
