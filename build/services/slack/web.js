"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const moleculer_1 = require("moleculer");
class SlackWebService extends moleculer_1.Service {
    constructor(broker) {
        super(broker);
        this.parseServiceSchema({
            name: "api.slack.web",
            version: 4,
            dependencies: [
                { name: "api.core", version: 4 }
            ],
            actions: {
                postMessage: {
                    name: "chat.post.message",
                    params: {
                        channel: {
                            type: "string",
                            empty: false,
                            trim: true
                        },
                        text: "string",
                        asUser: {
                            type: "boolean",
                            optional: true,
                            convert: true,
                            default: false
                        },
                        attachments: {
                            type: "array",
                            items: "object",
                            optional: true
                        },
                        blocks: {
                            type: "array",
                            items: "object",
                            optional: true
                        },
                        iconEmoji: {
                            type: "string",
                            optional: true,
                            trim: true
                        },
                        iconUrl: {
                            type: "url",
                            optional: true
                        },
                        linkNames: {
                            type: "boolean",
                            optional: true,
                            convert: true
                        },
                        mrkdwn: {
                            type: "boolean",
                            optional: true,
                            convert: true
                        },
                        parse: {
                            type: "string",
                            optional: true
                        },
                        replyBroadcast: {
                            type: "boolean",
                            convert: true,
                            optional: true
                        },
                        threadTs: {
                            type: "string",
                            convert: true,
                            optional: true,
                            trim: true
                        },
                        unfurlLinks: {
                            type: "boolean",
                            convert: true,
                            optional: true
                        },
                        unfurlMedia: {
                            type: "boolean",
                            convert: true,
                            optional: true
                        },
                        username: {
                            type: "string",
                            convert: true,
                            optional: true,
                            trim: true
                        }
                    },
                    handler: this.postMessage
                }
            }
        });
    }
    /**
     * Posts a message to the specified Slack channel.
     * @static
     * @function
     * @name chat.post.message
     * @param {string} channel - The channel to post the message to
     * @param {string} text - The text to post. Acts as a fallback if attachments or blocks are sent
     * @param {boolean} [asUser] - Whether to send this as the authorized user (if a user token is being used)
     * @param {any[]} [attachments] - An array of attachment objects
     * @param {any[]} [blocks] - An array of block objects
     * @param {string} [iconEmoji] - The emoji to use as the message icon. Overrides iconUrl. Must be used in conjunction with `asUser` set to `false`. Cannot be used with newer bot tokens
     * @param {string} [iconUrl] - The image to use as the message icon. Must be used in conjunction with `asUser` set to `false`. Cannot be used with newer bot tokens
     * @param {boolean} [linkNames] - Finds and links channel names and usernames
     * @param {boolean} [mrkdwn=true] - Enables/disables Slack markup parsing
     * @param {string} [parse] - Changes how messages are treated. [Check Slack documentation for more info](https://api.slack.com/methods/chat.postMessage#formatting)
     * @param {boolean} [replyBroadcast] - Used in conjunction with threadTs. Indicates whether the reply should be made visible to the main channel
     * @param {string} [threadTs] - Indicates which message this message should be in reply to
     * @param {boolean} [unfurlLinks] - Indicates whether unfurling of primarily text-based content should occur
     * @param {boolean} [unfurlMedia] - Indicates whether unfurling of media content should occur
     * @param {string} [username] - Sets the username for the message. Must be used in conjunction with `asUser` set to false
     * @return {object} Returns the Slack API response
     */
    async postMessage(ctx) {
        // TODO: Add error handling
        let slackResponse = await ctx.call("v4.api.slack.core.request", {
            endpoint: "chat.postMessage",
            method: "POST",
            json: {
                channel: ctx.params.channel,
                text: ctx.params.text,
                as_user: ctx.params.asUser || undefined,
                attachments: ctx.params.attachments || undefined,
                blocks: ctx.params.blocks || undefined,
                icon_emoji: ctx.params.iconEmoji || undefined,
                icon_url: ctx.params.iconUrl || undefined,
                link_names: ctx.params.linkNames || undefined,
                mrkdwn: ctx.params.mrkdwn || undefined,
                parse: ctx.params.parse || undefined,
                reply_broadcast: ctx.params.replyBroadcast || undefined,
                thread_ts: ctx.params.threadTs || undefined,
                unfurl_links: ctx.params.unfurlLinks || undefined,
                unfurl_media: ctx.params.unfurlMedia || undefined,
                username: ctx.params.username || undefined
            },
            tier: "message"
        });
        if (slackResponse.ok) {
            return slackResponse;
        }
        else {
            throw new moleculer_1.Errors.MoleculerClientError(slackResponse.error, 400, slackResponse.error.toUpperCase(), slackResponse);
        }
    }
}
exports.default = SlackWebService;
