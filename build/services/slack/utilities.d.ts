import { Service, Context } from "moleculer";
export default class SlackUtilitiesService extends Service {
    constructor(broker: any);
    markdownToMrkdwn(ctx: Context<{
        text: string;
    }, any>): Promise<any>;
}
