"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const moleculer_1 = require("moleculer");
const snuownd_1 = tslib_1.__importDefault(require("snuownd"));
// This apparently doesn't play nice with tslib
const htmlToMrkdwn = require("html-to-mrkdwn");
class SlackUtilitiesService extends moleculer_1.Service {
    constructor(broker) {
        super(broker);
        this.parseServiceSchema({
            name: "api.slack.utilities",
            version: 4,
            actions: {
                markdownToMrkdwn: {
                    name: "markdown-to-mrkdwn",
                    params: {
                        text: "string"
                    },
                    handler: this.markdownToMrkdwn
                }
            }
        });
    }
    async markdownToMrkdwn(ctx) {
        const html = snuownd_1.default.getParser().render(ctx.params.text);
        return htmlToMrkdwn(html).text;
    }
}
exports.default = SlackUtilitiesService;
