import { Service, Context } from "moleculer";
import Bottleneck from "bottleneck";
export default class SlackCoreService extends Service {
    rateLimiter: {
        t1: Bottleneck;
        t2: Bottleneck;
        t3: Bottleneck;
        t4: Bottleneck;
        message: Bottleneck;
    };
    constructor(broker: any);
    testApi(ctx: Context<{}, any>): Promise<unknown>;
    request(ctx: Context<{
        endpoint: string;
        method: string;
        qs?: any;
        form?: any;
        json?: any;
        tier: "t1" | "t2" | "t3" | "t4" | "message";
    }, any>): Promise<unknown>;
    serviceCreated(): void;
    serviceStopped(): Promise<void>;
}
