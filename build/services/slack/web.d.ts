import { Service, Context } from "moleculer";
export default class SlackWebService extends Service {
    constructor(broker: any);
    /**
     * Posts a message to the specified Slack channel.
     * @static
     * @function
     * @name chat.post.message
     * @param {string} channel - The channel to post the message to
     * @param {string} text - The text to post. Acts as a fallback if attachments or blocks are sent
     * @param {boolean} [asUser] - Whether to send this as the authorized user (if a user token is being used)
     * @param {any[]} [attachments] - An array of attachment objects
     * @param {any[]} [blocks] - An array of block objects
     * @param {string} [iconEmoji] - The emoji to use as the message icon. Overrides iconUrl. Must be used in conjunction with `asUser` set to `false`. Cannot be used with newer bot tokens
     * @param {string} [iconUrl] - The image to use as the message icon. Must be used in conjunction with `asUser` set to `false`. Cannot be used with newer bot tokens
     * @param {boolean} [linkNames] - Finds and links channel names and usernames
     * @param {boolean} [mrkdwn=true] - Enables/disables Slack markup parsing
     * @param {string} [parse] - Changes how messages are treated. [Check Slack documentation for more info](https://api.slack.com/methods/chat.postMessage#formatting)
     * @param {boolean} [replyBroadcast] - Used in conjunction with threadTs. Indicates whether the reply should be made visible to the main channel
     * @param {string} [threadTs] - Indicates which message this message should be in reply to
     * @param {boolean} [unfurlLinks] - Indicates whether unfurling of primarily text-based content should occur
     * @param {boolean} [unfurlMedia] - Indicates whether unfurling of media content should occur
     * @param {string} [username] - Sets the username for the message. Must be used in conjunction with `asUser` set to false
     * @return {object} Returns the Slack API response
     */
    postMessage(ctx: Context<{
        channel: string;
        text: string;
        asUser?: boolean;
        attachments?: any[];
        blocks?: any[];
        iconEmoji?: string;
        iconUrl?: string;
        linkNames?: boolean;
        mrkdwn?: boolean;
        parse?: string;
        replyBroadcast?: boolean;
        threadTs?: string;
        unfurlLinks?: boolean;
        unfurlMedia?: boolean;
        username?: string;
    }, any>): Promise<any>;
}
