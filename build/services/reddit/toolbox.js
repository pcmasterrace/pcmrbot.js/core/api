"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const moleculer_1 = require("moleculer");
class RedditToolboxService extends moleculer_1.Service {
    constructor(broker) {
        super(broker);
        this.parseServiceSchema({
            name: "api.reddit.toolbox",
            version: 4,
            dependencies: [
                { name: "api.reddit.wiki", version: 4 }
            ],
            actions: {
                getConfig: {
                    name: "config",
                    params: {
                        subreddit: "string"
                    },
                    cache: {
                        ttl: 7200
                    },
                    handler: this.getConfig
                },
                renderRemovalReason: {
                    name: "config.render",
                    params: {
                        subreddit: "string",
                        reasons: {
                            type: "array",
                            items: "string",
                            default: [],
                            optional: true
                        },
                        customReason: {
                            type: "string",
                            optional: true
                        },
                        useHeader: {
                            type: "boolean",
                            convert: true,
                            default: true,
                            optional: true
                        },
                        useFooter: {
                            type: "boolean",
                            convert: true,
                            default: true,
                            optional: true
                        },
                        author: {
                            type: "string",
                            optional: true,
                            convert: true,
                            trim: true,
                            empty: false
                        },
                        kind: {
                            type: "string",
                            optional: true,
                            convert: true,
                            trim: true,
                            empty: false
                        },
                        mod: {
                            type: "string",
                            optional: true,
                            convert: true,
                            trim: true,
                            empty: false
                        },
                        title: {
                            type: "string",
                            optional: true,
                            convert: true,
                            trim: true,
                            empty: false
                        },
                        url: {
                            type: "string",
                            optional: true,
                            convert: true,
                            trim: true,
                            empty: false
                        },
                        domain: {
                            type: "string",
                            optional: true,
                            convert: true,
                            trim: true,
                            empty: false
                        },
                        link: {
                            type: "string",
                            optional: true,
                            convert: true,
                            trim: true,
                            empty: false
                        }
                    },
                    handler: this.renderRemovalMessage
                }
            }
        });
    }
    /**
     * Gets and unescapes the Toolbox config from Reddit.
     * @static
     * @function
     * @name config
     * @param {string} subreddit - The subreddit to retrieve the config from
     * @returns {Typings.Reddit.ToolboxConfig} Returns a ToolboxConfig object
     */
    async getConfig(ctx) {
        let config = await ctx.call("v4.api.reddit.wiki.page.get", {
            subreddit: ctx.params.subreddit,
            page: "toolbox"
        });
        // @ts-ignore
        config = JSON.parse(config.body);
        // Async decode these for reasons
        let macros = config.modMacros.map(async (macro) => {
            macro.text = unescape(macro.text);
            return macro;
        });
        let reasons = config.removalReasons.reasons.map(async (reason) => {
            reason.text = unescape(reason.text);
            return reason;
        });
        let escaped = await Promise.all([Promise.all(macros), Promise.all(reasons)]);
        config.modMacros = escaped[0];
        config.removalReasons.header = unescape(config.removalReasons.header);
        config.removalReasons.reasons = escaped[1];
        config.removalReasons.footer = unescape(config.removalReasons.footer);
        return config;
    }
    /**
     * Renders a removal message.
     * @static
     * @function
     * @name config.render
     * @param {string} subreddit - The subreddit to retrieve the config from
     * @param {string[]} [reasons=[]] - The reasons to render in the post
     * @param {string} [customReason] - A custom reason to add
     * @param {boolean} [useHeader=true] - Whether to render the reason header
     * @param {boolean} [useFooter=true] - Whether to render the reason footer
     * @param {string} [author] - The value to replace {{author}} tags with
     * @param {string} [kind] - The value to replace {{kind}} tags with
     * @param {string} [mod] - The value to replace {{mod}} tags with
     * @param {string} [title] - The value to replace {{title}} tags with
     * @param {string} [url] - The value to replace {{url}} tags with
     * @param {string} [domain] - The value to replace {{domain}} tags with
     * @param {string} [link] - The value to replace {{link}} tags with
     */
    async renderRemovalMessage(ctx) {
        const tbConfig = await ctx.call("v4.api.reddit.toolbox.config", {
            subreddit: ctx.params.subreddit
        });
        const reasons = tbConfig.removalReasons.reasons;
        let message = "";
        if (ctx.params.useHeader)
            message += tbConfig.removalReasons.header + "\n\n";
        for (let reasonTitle of ctx.params.reasons) {
            let matchingReasons = reasons.filter(reason => {
                return reason.title.toLowerCase().includes(reasonTitle.toLowerCase());
            });
            message += matchingReasons[0].text + "\n";
        }
        if (ctx.params.customReason)
            message += ctx.params.customReason + "\n";
        if (ctx.params.useFooter)
            message += "\n" + tbConfig.removalReasons.footer;
        message = message.replace(/\{subreddit\}/g, ctx.params.subreddit);
        message = message.replace(/\{author\}/g, ctx.params.author);
        message = message.replace(/\{kind\}/g, ctx.params.kind);
        message = message.replace(/\{mod\}/g, ctx.params.mod);
        message = message.replace(/\{title\}/g, ctx.params.title);
        message = message.replace(/\{url\}/g, ctx.params.url);
        message = message.replace(/\{domain\}/g, ctx.params.domain);
        message = message.replace(/\{link\}/g, ctx.params.link);
        return message;
    }
}
exports.default = RedditToolboxService;
