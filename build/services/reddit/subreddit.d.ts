import { Service, Context } from "moleculer";
import { Typings } from "@pcmrbotjs/core-typings/";
export default class SubredditService extends Service {
    constructor(broker: any);
    /**
     * Retrieves the content of the specified subreddit's /hot index.
     * @static
     * @function
     * @name index.hot
     * @param {string} subreddit - The subreddit to fetch
     * @param {string} [after] - The item to start retrieving from. All retrieved items will be **older** than this
     * @param {string} [before] - The item to retrieve up to. All retrieved items will be **newer** than this
     * @param {number} [limit] - The maximum number of items to return in this slice of the listing
     * @param {number} [count] - The number of items already seen in the listing
     * @returns {Reddit.SubredditIndex} Returns an array of Post objects
     */
    getHot(ctx: Context<{
        subreddit: string;
        after?: string;
        before?: string;
        count?: number;
        limit?: number;
    }, any>): Promise<Typings.Reddit.SubredditIndex>;
    /**
     * Retrieves the content of the specified subreddit's /new index.
     * @static
     * @function
     * @name index.new
     * @param {string} subreddit - The subreddit to fetch
     * @param {string} [after] - The item to start retrieving from. All retrieved items will be **older** than this
     * @param {string} [before] - The item to retrieve up to. All retrieved items will be **newer** than this
     * @param {number} [limit] - The maximum number of items to return in this slice of the listing
     * @param {number} [count] - The number of items already seen in the listing
     * @returns {Reddit.SubredditIndex} Returns an array of Post objects
     */
    getNew(ctx: Context<{
        subreddit: string;
        after?: string;
        before?: string;
        count?: number;
        limit?: number;
    }, any>): Promise<Typings.Reddit.SubredditIndex>;
    /**
     * Retrieves the content of the specified subreddit's /rising index.
     * @static
     * @function
     * @name index.rising
     * @param {string} subreddit - The subreddit to fetch
     * @param {string} [after] - The item to start retrieving from. All retrieved items will be **older** than this
     * @param {string} [before] - The item to retrieve up to. All retrieved items will be **newer** than this
     * @param {number} [limit] - The maximum number of items to return in this slice of the listing
     * @param {number} [count] - The number of items already seen in the listing
     * @returns {Reddit.SubredditIndex} Returns an array of Post objects
     */
    getRising(ctx: Context<{
        subreddit: string;
        after?: string;
        before?: string;
        count?: number;
        limit?: number;
    }, any>): Promise<Typings.Reddit.SubredditIndex>;
    /**
     * Retrieves the content of the specified subreddit's /controversial index.
     * @static
     * @function
     * @name index.controversial
     * @param {string} subreddit - The subreddit to fetch
     * @param {string} [after] - The item to start retrieving from. All retrieved items will be **older** than this
     * @param {string} [before] - The item to retrieve up to. All retrieved items will be **newer** than this
     * @param {number} [limit] - The maximum number of items to return in this slice of the listing
     * @param {number} [count] - The number of items already seen in the listing
     * @param {"hour" | "day" | "week" | "month" | "year" | "all"} time - The time period to retrieve
     * @returns {Reddit.SubredditIndex} Returns an array of Post objects
     */
    getControversial(ctx: Context<{
        subreddit: string;
        after?: string;
        before?: string;
        count?: number;
        limit?: number;
        time?: "hour" | "day" | "week" | "month" | "year" | "all";
    }, any>): Promise<Typings.Reddit.SubredditIndex>;
    /**
     * Retrieves the content of the specified subreddit's /top index.
     * @static
     * @function
     * @name index.top
     * @param {string} subreddit - The subreddit to fetch
     * @param {string} [after] - The item to start retrieving from. All retrieved items will be **older** than this
     * @param {string} [before] - The item to retrieve up to. All retrieved items will be **newer** than this
     * @param {number} [limit] - The maximum number of items to return in this slice of the listing
     * @param {number} [count] - The number of items already seen in the listing
     * @param {"hour" | "day" | "week" | "month" | "year" | "all"} time - The time period to retrieve
     * @returns {Reddit.SubredditIndex} Returns an array of Post objects
     */
    getTop(ctx: Context<{
        subreddit: string;
        after?: string;
        before?: string;
        count?: number;
        limit?: number;
        time?: "hour" | "day" | "week" | "month" | "year" | "all";
    }, any>): Promise<Typings.Reddit.SubredditIndex>;
    /**
     * Retrieves the content of the specified subreddit's /comments index.
     * @static
     * @function
     * @name comments
     * @param {string} subreddit - The subreddit to fetch
     * @param {string} [after] - The item to start retrieving from. All retrieved items will be **older** than this
     * @param {string} [before] - The item to retrieve up to. All retrieved items will be **newer** than this
     * @param {number} [limit] - The maximum number of items to return in this slice of the listing
     * @param {number} [count] - The number of items already seen in the listing
     * @returns {Reddit.Comment[]} Returns an array of Post objects
     */
    getComments(ctx: Context<{
        subreddit: string;
        after?: string;
        before?: string;
        count?: number;
        limit?: number;
    }, any>): Promise<Typings.Reddit.Comment[]>;
    /**
     * Retrieves the subreddit information and formats it for proper consumption.
     * @static
     * @function
     * @name about
     * @param {string} subreddit - The subreddit to retrieve information for
     * @returns {Reddit.Subreddit} Returns a Subreddit object
     */
    getAbout(ctx: Context<{
        subreddit: string;
    }, any>): Promise<Typings.Reddit.Subreddit>;
}
