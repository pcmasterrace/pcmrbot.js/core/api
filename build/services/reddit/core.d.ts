import { Service, Context } from "moleculer";
import Bottleneck from "bottleneck";
export default class RedditCoreService extends Service {
    rateLimiter: Bottleneck;
    constructor(broker: any);
    request(ctx: Context<{
        endpoint: string;
        method: string;
        qs?: any;
        form?: any;
    }, {
        serviceType?: string;
        userId?: string;
    }>): Promise<unknown>;
    serviceCreated(): void;
    serviceStopped(): Promise<void>;
}
