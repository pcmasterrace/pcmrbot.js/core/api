"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const moleculer_1 = require("moleculer");
const RedditPostService = require("./post");
class RedditCommentService extends moleculer_1.Service {
    constructor(broker) {
        super(broker);
        this.parseServiceSchema({
            name: "api.reddit.comment",
            version: 4,
            mixins: [RedditPostService]
        });
    }
}
exports.default = RedditCommentService;
