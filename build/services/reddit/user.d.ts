import { Service, Context } from "moleculer";
export default class RedditUserService extends Service {
    constructor(broker: any);
    /**
     * Sends a private message to a user.
     * @static
     * @function
     * @name message
     * @param {string} to - The user to send the message to
     * @param {string} subject - The message subject. Must be under 100 characters
     * @param {string} text - The message text
     * @param {string} [fromSr] - The subreddit name to send the message from. Sends from the current user account if left undefined
     * @returns idk
     */
    sendMessage(ctx: Context<{
        to: string;
        fromSr?: string;
        subject: string;
        text: string;
    }, any>): Promise<unknown>;
}
