"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const moleculer_1 = require("moleculer");
class RedditUserService extends moleculer_1.Service {
    constructor(broker) {
        super(broker);
        this.parseServiceSchema({
            name: "api.reddit.user",
            version: 4,
            dependencies: [
                { name: "api.reddit.core", version: 4 }
            ],
            actions: {
                sendMessage: {
                    name: "message",
                    params: {
                        to: {
                            type: "string",
                            trim: true,
                            convert: true,
                            empty: false
                        },
                        fromSr: {
                            type: "string",
                            optional: true
                        },
                        subject: {
                            type: "string",
                            max: 100,
                            empty: false,
                            convert: true,
                            trim: true
                        },
                        text: {
                            type: "string",
                            empty: false,
                            convert: true
                        }
                    },
                    handler: this.sendMessage
                }
            }
        });
    }
    /**
     * Sends a private message to a user.
     * @static
     * @function
     * @name message
     * @param {string} to - The user to send the message to
     * @param {string} subject - The message subject. Must be under 100 characters
     * @param {string} text - The message text
     * @param {string} [fromSr] - The subreddit name to send the message from. Sends from the current user account if left undefined
     * @returns idk
     */
    async sendMessage(ctx) {
        return await ctx.call("v4.api.reddit.core.request", {
            endpoint: `/api/compose`,
            method: "POST",
            form: {
                api_type: "json",
                to: ctx.params.to,
                subject: ctx.params.subject,
                text: ctx.params.text,
                from_sr: ctx.params.fromSr || undefined
            }
        });
    }
}
exports.default = RedditUserService;
