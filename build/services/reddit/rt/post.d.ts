import { Service, Context } from "moleculer";
import { Sequelize } from "sequelize";
import { ModelsInterface } from "../../../models/index";
export default class RedditRtPostService extends Service {
    db: Sequelize;
    models: ModelsInterface;
    constructor(broker: any);
    pollReddit(ctx: Context<{
        subreddit: string;
    }, any>): Promise<void>;
    serviceCreated(): void;
    serviceStarted(): Promise<void>;
}
