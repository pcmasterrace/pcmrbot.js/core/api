"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const moleculer_1 = require("moleculer");
const ApiDatabase = require("../../database");
class RedditRtPostService extends moleculer_1.Service {
    constructor(broker) {
        super(broker);
        this.parseServiceSchema({
            name: "api.reddit.rt.post",
            version: 4,
            mixins: [ApiDatabase],
            dependencies: [
                { name: "api.reddit.subreddit", version: 4 }
            ],
            settings: {
                subreddit: process.env.API_REDDIT_RT_SUBREDDIT,
                interval: Number(process.env.API_REDDIT_RT_INTERVAL) * 1000 || 10000
            },
            actions: {
                pollReddit: {
                    name: "poll",
                    params: {
                        subreddit: "string"
                    },
                    handler: this.pollReddit
                }
            },
            created: this.serviceCreated,
            started: this.serviceStarted
        });
    }
    async pollReddit(ctx) {
        // Fetch new posts from Reddit
        const newSubmissions = await ctx.call("v4.api.reddit.subreddit.index.new", {
            subreddit: ctx.params.subreddit,
            limit: 100
        });
        const newComments = await ctx.call("v4.api.reddit.subreddit.comments", {
            subreddit: ctx.params.subreddit,
            limit: 100
        });
        // Async for reasons
        newSubmissions.map(async (submission) => {
            const [name, created] = await this.models.ApiRedditRtSeenPost.findOrCreate({ where: {
                    id: submission.name
                } });
            if (created) {
                ctx.emit("v4.api.reddit.rt.post.submission", submission);
            }
        });
        newComments.map(async (comment) => {
            const [name, created] = await this.models.ApiRedditRtSeenPost.findOrCreate({ where: {
                    id: comment.name
                } });
            if (created) {
                ctx.emit("v4.api.reddit.rt.post.comment", comment);
            }
        });
    }
    serviceCreated() {
        if (!this.settings.subreddit)
            this.broker.fatal("Reddit RT subreddit not supplied! Make sure API_REDDIT_RT_SUBREDDIT is populated!");
    }
    async serviceStarted() {
        setInterval(() => this.broker.call("v4.api.reddit.rt.post.poll", {
            subreddit: this.settings.subreddit
        }), this.settings.interval);
    }
}
exports.default = RedditRtPostService;
