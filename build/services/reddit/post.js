"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const moleculer_1 = require("moleculer");
const moment_timezone_1 = tslib_1.__importDefault(require("moment-timezone"));
const fastest_validator_1 = tslib_1.__importDefault(require("fastest-validator"));
const core_typings_1 = require("@pcmrbotjs/core-typings");
const CommentValidator = new fastest_validator_1.default().compile(core_typings_1.FastestValidator.Reddit.Comment);
const SubmissionValidator = new fastest_validator_1.default().compile(core_typings_1.FastestValidator.Reddit.Submission);
// This is a ServiceSchema instead of a regular ES6 class so I can import it as a mixin
module.exports = {
    name: "api.reddit.post",
    version: 4,
    dependencies: [
        { name: "api.reddit.core", version: 4 }
    ],
    actions: {
        get: {
            name: "get",
            params: {
                postName: {
                    type: "string",
                    empty: false,
                    convert: true,
                    trim: true
                },
                commentName: {
                    type: "string",
                    empty: false,
                    convert: true,
                    optional: true
                },
                context: {
                    type: "number",
                    integer: true,
                    min: 0,
                    max: 8,
                    optional: true
                },
                depth: {
                    type: "number",
                    convert: true,
                    optional: true,
                    min: 0,
                    max: 8,
                    integer: true
                },
                limit: {
                    type: "number",
                    convert: true,
                    optional: true,
                    integer: true
                },
                showEdits: {
                    type: "boolean",
                    optional: true,
                    convert: true,
                    default: true
                },
                showMore: {
                    type: "boolean",
                    optional: true,
                    convert: true,
                    default: true
                },
                sort: {
                    type: "enum",
                    values: ["confidence", "top", "new", "controversial", "old", "random", "qa", "live"],
                    optional: true,
                    default: "confidence"
                },
                threaded: {
                    type: "boolean",
                    convert: true,
                    optional: true
                },
                truncate: {
                    type: "number",
                    convert: true,
                    min: 0,
                    max: 50,
                    integer: true,
                    optional: true
                }
            },
            async handler(ctx) {
                // Strip the thing type from the post name
                let postName;
                if (ctx.params.postName.startsWith("t3_")) {
                    postName = ctx.params.postName.substr(3);
                }
                else {
                    postName = ctx.params.postName;
                }
                let comment = undefined;
                if (ctx.params.commentName && ctx.params.commentName.startsWith("t1_")) {
                    comment = ctx.params.commentName.substr(3);
                }
                else if (ctx.params.commentName) {
                    comment = ctx.params.commentName;
                }
                let redditResponse = await ctx.call("v4.api.reddit.core.request", {
                    endpoint: `/comments/${postName}`,
                    method: "GET",
                    qs: {
                        comment: comment,
                        context: ctx.params.context || undefined,
                        depth: ctx.params.depth || undefined,
                        limit: ctx.params.limit || undefined,
                        showedits: ctx.params.showEdits || undefined,
                        showmore: ctx.params.showMore || undefined,
                        sort: ctx.params.sort || undefined,
                        threaded: ctx.params.threaded || undefined,
                        truncate: ctx.params.truncate || undefined
                    }
                });
                let response = {
                    submission: await ctx.call("v4.api.reddit.post.sanitize.t3", redditResponse[0].data.children[0]),
                    comments: []
                };
                for (let rawComment of redditResponse[1].data.children) {
                    response.comments.push(await ctx.call("v4.api.reddit.post.sanitize.t1", rawComment));
                }
                return response;
            }
        },
        getByName: {
            name: "get.name",
            params: {
                names: [
                    {
                        type: "string",
                        empty: false,
                        trim: true
                    },
                    {
                        type: "array",
                        items: {
                            type: "string",
                            empty: false,
                            trim: true
                        }
                    }
                ]
            },
            async handler(ctx) {
                let redditResponse = await ctx.call("v4.api.reddit.core.request", {
                    endpoint: `/by_id/${Array.isArray(ctx.params.names) ? ctx.params.names.join(",") : ctx.params.names}`,
                    method: "GET"
                });
                let response = [];
                for (let thing of redditResponse.data.children) {
                    response.push(await ctx.call("v4.api.reddit.post.sanitize", thing));
                }
                return response;
            }
        },
        reply: {
            name: "reply",
            params: {
                postName: {
                    type: "string",
                    empty: false,
                    trim: true,
                    convert: true
                },
                body: {
                    type: "string",
                    empty: false,
                    convert: true
                }
            },
            async handler(ctx) {
                let redditResponse = await ctx.call("v4.api.reddit.core.request", {
                    endpoint: "/api/comment",
                    method: "POST",
                    form: {
                        api_type: "json",
                        thing_id: ctx.params.postName,
                        text: ctx.params.body
                    }
                });
                if (redditResponse.json.errors.length === 0) {
                    return await ctx.call("v4.api.reddit.post.sanitize.t1", redditResponse.json.data.things[0]);
                }
                else {
                    throw new moleculer_1.Errors.MoleculerError("I don't know what went wrong", 400, "IDK", redditResponse);
                }
            }
        },
        approve: {
            name: "approve",
            params: {
                postName: {
                    type: "string",
                    empty: false,
                    trim: true,
                    convert: true
                },
            },
            async handler(ctx) {
                return await ctx.call("v4.api.reddit.core.request", {
                    endpoint: "/api/approve",
                    method: "POST",
                    form: {
                        id: ctx.params.postName
                    }
                });
            }
        },
        remove: {
            name: "remove",
            params: {
                postName: {
                    type: "string",
                    empty: false,
                    trim: true,
                    convert: true
                },
                spam: {
                    type: "boolean",
                    convert: true,
                    default: false,
                    optional: true
                }
            },
            async handler(ctx) {
                return await ctx.call("v4.api.reddit.core.request", {
                    endpoint: "/api/remove",
                    method: "POST",
                    form: {
                        id: ctx.params.postName,
                        spam: ctx.params.spam
                    }
                });
            }
        },
        distinguish: {
            name: "distinguish",
            params: {
                distinguish: {
                    type: "boolean",
                    convert: true
                },
                postName: {
                    type: "string",
                    empty: false,
                    trim: true,
                    convert: true
                },
                sticky: {
                    type: "boolean",
                    default: false,
                    optional: true,
                    convert: true
                }
            },
            async handler(ctx) {
                const redditResponse = await ctx.call("v4.api.reddit.core.request", {
                    endpoint: "/api/distinguish",
                    method: "POST",
                    form: {
                        api_type: "json",
                        id: ctx.params.postName,
                        how: ctx.params.distinguish ? "yes" : "no",
                        sticky: ctx.params.sticky || undefined
                    }
                });
                if (redditResponse.json.errors.length === 0) {
                    switch (redditResponse.json.data.things[0].kind) {
                        case "t1":
                            return await ctx.call("v4.api.reddit.post.sanitize.t1", redditResponse.json.data.things[0]);
                        case "t3":
                            return await ctx.call("v4.api.reddit.post.sanitize.t3", redditResponse.json.data.things[0]);
                        default:
                            throw new moleculer_1.Errors.MoleculerError("Reddit returned non-comment/non-submission object", 500, "IDK", redditResponse);
                    }
                }
                else {
                    throw new moleculer_1.Errors.MoleculerError("I don't know what went wrong", 400, "IDK", redditResponse);
                }
            }
        },
        sanitizeGeneral: {
            name: "sanitize",
            params: {
                kind: {
                    type: "enum",
                    values: ["t1", "t2", "t3", "t4", "t5", "t6"]
                },
                data: "object"
            },
            async handler(ctx) {
                if (ctx.params.kind === "t1") {
                    return await ctx.call("v4.api.reddit.post.sanitize.t1", ctx.params);
                }
                else if (ctx.params.kind === "t3") {
                    return await ctx.call("v4.api.reddit.post.sanitize.t3", ctx.params);
                }
                else {
                    return new moleculer_1.Errors.MoleculerClientError("Unsupported thing type", 400, "INVALID_PARAM", { kind: ctx.params.kind });
                }
            }
        },
        sanitizeComment: {
            name: "sanitize.t1",
            params: {
                kind: {
                    type: "enum",
                    values: ["t1"]
                },
                data: "object"
            },
            async handler(ctx) {
                let comment = {
                    approved: ctx.params.data.approved,
                    approvedAt: ctx.params.data.approved_at_utc ? moment_timezone_1.default.utc(ctx.params.data.approved_at_utc * 1000).toDate() : null,
                    approvedBy: ctx.params.data.approved_by,
                    archived: ctx.params.data.archived,
                    author: {
                        flair: {
                            cssClass: ctx.params.data.author_flair_css_class,
                            templateId: ctx.params.data.author_flair_template_id,
                            text: ctx.params.data.author_flair_text
                        },
                        name: ctx.params.data.author_fullname || null,
                        isSubmitter: ctx.params.data.is_submitter,
                        username: ctx.params.data.author
                    },
                    awards: ctx.params.data.all_awardings,
                    body: ctx.params.data.body,
                    created: moment_timezone_1.default.utc(ctx.params.data.created_utc * 1000).toDate(),
                    distinguished: ctx.params.data.distinguished ? true : false,
                    edited: ctx.params.data.edited === false ? false : true,
                    editedAt: ctx.params.data.edited === true ? moment_timezone_1.default.utc(ctx.params.data.edited * 1000).toDate() : null,
                    locked: ctx.params.data.locked,
                    modNote: {
                        author: ctx.params.data.mod_reason_by,
                        note: ctx.params.data.mod_note,
                        title: ctx.params.data.mod_reason_title
                    },
                    name: ctx.params.data.name,
                    nsfw: ctx.params.data.over_18 || false,
                    parentName: ctx.params.data.parent_id,
                    parentSubmission: {
                        author: ctx.params.data.link_author || null,
                        comments: ctx.params.data.num_comments || null,
                        name: ctx.params.data.link_id || null,
                        permalink: ctx.params.data.link_permalink || null,
                        title: ctx.params.data.link_title || null,
                        url: ctx.params.data.link_url || null
                    },
                    permalink: ctx.params.data.permalink,
                    removalReason: ctx.params.data.removal_reason,
                    removed: ctx.params.data.removed,
                    removedAt: ctx.params.data.banned_at_utc ? moment_timezone_1.default.utc(ctx.params.data.banned_at_utc * 1000).toDate() : null,
                    removedBy: ctx.params.data.banned_by,
                    reports: {
                        count: 0,
                        dismissed: [],
                        ignore: ctx.params.data.ignore_reports || false,
                        mod: [],
                        user: []
                    },
                    replies: [],
                    score: ctx.params.data.score || 0,
                    scoreHidden: ctx.params.data.score_hidden,
                    spam: ctx.params.data.spam,
                    stickied: ctx.params.data.stickied,
                    subreddit: ctx.params.data.subreddit
                };
                if (ctx.params.data.user_reports_dismissed) {
                    for (let report of ctx.params.data.user_reports_dismissed) {
                        comment.reports.dismissed.push({
                            count: report[1],
                            reason: report[0]
                        });
                        comment.reports.count += report[1];
                    }
                }
                // Convert reports
                for (let report of ctx.params.data.user_reports) {
                    comment.reports.user.push({
                        count: report[1],
                        reason: report[0]
                    });
                    comment.reports.count += report[1];
                }
                for (let report of ctx.params.data.mod_reports) {
                    comment.reports.mod.push({
                        mod: report[1],
                        reason: report[0]
                    });
                    comment.reports.count += 1;
                }
                // Parse replies
                if (ctx.params.data.replies !== "") {
                    for (let reply of ctx.params.data.replies.data.children) {
                        comment.replies.push(await ctx.call("v4.api.reddit.post.sanitize.t1", reply));
                    }
                }
                // Verify that this passes validation before sending it out
                if (CommentValidator(comment) === true) {
                    return comment;
                }
                else {
                    this.logger.warn("Comment failed validation! Details below:\n\n", JSON.stringify({ params: ctx.params, errors: CommentValidator(comment) }, null, 4));
                    throw new moleculer_1.Errors.MoleculerRetryableError("Comment failed validation!", 503, "VALIDATION_FAIL", {
                        params: ctx.params,
                        errors: CommentValidator(comment)
                    });
                }
            }
        },
        sanitizeSubmission: {
            name: "sanitize.t3",
            params: {
                kind: {
                    type: "enum",
                    values: ["t3"]
                },
                data: "object"
            },
            async handler(ctx) {
                let submission = {
                    approved: ctx.params.data.approved,
                    approvedBy: ctx.params.data.approved_by,
                    approvedAt: ctx.params.data.approved_at_utc ? moment_timezone_1.default.utc(ctx.params.data.approved_at_utc * 1000).toDate() : null,
                    archived: ctx.params.data.archived,
                    author: {
                        flair: {
                            cssClass: ctx.params.data.author_flair_css_class,
                            templateId: ctx.params.data.author_flair_template_id,
                            text: ctx.params.data.author_flair_text,
                        },
                        name: ctx.params.data.author_fullname || null,
                        username: ctx.params.data.author,
                    },
                    awards: ctx.params.data.all_awardings,
                    body: ctx.params.data.selftext || "",
                    commentCount: ctx.params.data.num_comments,
                    contestMode: ctx.params.data.contest_mode,
                    created: moment_timezone_1.default.utc(ctx.params.data.created_utc * 1000).toDate(),
                    distinguished: ctx.params.data.distinguished ? true : false,
                    domain: ctx.params.data.domain,
                    edited: ctx.params.data.edited === false ? false : true,
                    editedAt: ctx.params.data.edited === true ? moment_timezone_1.default.utc(ctx.params.data.edited * 1000).toDate() : null,
                    flair: {
                        cssClass: ctx.params.data.link_flair_css_class,
                        templateId: ctx.params.data.link_flair_template_id,
                        text: ctx.params.data.link_flair_text,
                    },
                    gilded: ctx.params.data.gilded,
                    isSelf: ctx.params.data.is_self,
                    isVideo: ctx.params.data.is_video,
                    locked: ctx.params.data.locked,
                    media: ctx.params.data.media ? {
                        embed: ctx.params.data.oembed ? {
                            author: {
                                name: ctx.params.data.media.oembed.author_name || "",
                                url: ctx.params.data.media.oembed.author_url || ""
                            },
                            provider: {
                                name: ctx.params.data.media.oembed.provider_name || "",
                                url: ctx.params.data.media.oembed.provider_url || ""
                            },
                            thumbnailUrl: ctx.params.data.media.oembed.thumbnail_url || "",
                            title: ctx.params.data.media.oembed.title || "",
                            type: ctx.params.data.media.oembed.type || ""
                        } : null,
                        redditVideo: ctx.params.data.is_video ? {
                            dashUrl: ctx.params.data.media.reddit_video.dash_url,
                            duration: ctx.params.data.media.reddit_video.duration,
                            fallbackUrl: ctx.params.data.media.reddit_video.fallback_url,
                            hlsUrl: ctx.params.data.media.reddit_video.hlsUrl,
                            isGif: ctx.params.data.media.reddit_video.is_gif,
                            transcodingStatus: ctx.params.data.media.reddit_video.transcoding_status
                        } : null,
                        type: ctx.params.data.media.type || "",
                    } : null,
                    modNote: {
                        author: ctx.params.data.mod_reason_by,
                        note: ctx.params.data.mod_note,
                        title: ctx.params.data.mod_reason_title
                    },
                    name: ctx.params.data.name,
                    nsfw: ctx.params.data.over_18,
                    permalink: ctx.params.data.permalink,
                    pinned: ctx.params.data.pinned,
                    preview: ctx.params.data.preview || null,
                    removalReason: ctx.params.data.removal_reason,
                    removed: ctx.params.data.removed,
                    removedAt: ctx.params.data.banned_at_utc ? moment_timezone_1.default.utc(ctx.params.data.banned_at_utc * 1000).toDate() : null,
                    removedBy: ctx.params.data.banned_by,
                    reports: {
                        count: 0,
                        dismissed: [],
                        ignore: ctx.params.data.ignore_reports || false,
                        mod: [],
                        user: []
                    },
                    score: ctx.params.data.score || 0,
                    spam: ctx.params.data.spam,
                    spoiler: ctx.params.data.spoiler,
                    stickied: ctx.params.data.stickied,
                    subreddit: ctx.params.data.subreddit,
                    suggestedSort: ctx.params.data.suggessted_sort,
                    thumbnail: ctx.params.data.thumbnail,
                    title: ctx.params.data.title,
                    url: ctx.params.data.url || null,
                    viewCount: ctx.params.data.view_count || 0,
                };
                if (ctx.params.data.user_reports_dismissed) {
                    for (let report of ctx.params.data.user_reports_dismissed) {
                        submission.reports.dismissed.push({
                            count: report[1],
                            reason: report[0]
                        });
                        submission.reports.count += report[1];
                    }
                }
                for (let report of ctx.params.data.user_reports) {
                    submission.reports.user.push({
                        count: report[1],
                        reason: report[0]
                    });
                    submission.reports.count += report[1];
                }
                for (let report of ctx.params.data.mod_reports) {
                    submission.reports.mod.push({
                        mod: report[1],
                        reason: report[0]
                    });
                    submission.reports.count += 1;
                }
                // Verify that this passes validation before sending it out
                if (SubmissionValidator(submission) === true) {
                    return submission;
                }
                else {
                    this.logger.warn("Submission failed validation! Details below:\n", JSON.stringify({ params: ctx.params, errors: SubmissionValidator(submission) }, null, 4));
                    throw new moleculer_1.Errors.MoleculerRetryableError("Submission failed validation!", 503, "VALIDATION_FAIL", {
                        params: ctx.params,
                        errors: SubmissionValidator(submission)
                    });
                }
            }
        }
    }
};
