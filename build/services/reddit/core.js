"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const moleculer_1 = require("moleculer");
const bottleneck_1 = tslib_1.__importDefault(require("bottleneck"));
class RedditCoreService extends moleculer_1.Service {
    constructor(broker) {
        super(broker);
        this.parseServiceSchema({
            name: "api.reddit.core",
            version: 4,
            dependencies: [
                { name: "api.core", version: 4 },
                { name: "accounts", version: 4 }
            ],
            settings: {
                bottleneck: {
                    redis: {
                        host: process.env.API_REDDIT_REDIS_HOST,
                        port: Number(process.env.API_REDDIT_REDIS_PORT) || 6379
                    }
                },
                reddit: {
                    oauth: {
                        userAgent: process.env.API_REDDIT_OAUTH_USERAGENT
                    }
                }
            },
            actions: {
                request: {
                    name: "request",
                    params: {
                        endpoint: {
                            type: "string",
                            empty: false,
                            trim: true,
                            convert: true
                        },
                        method: {
                            type: "string",
                            convert: true
                        },
                        qs: {
                            type: "any",
                            optional: true
                        },
                        form: {
                            type: "object",
                            optional: true
                        }
                    },
                    handler: this.request
                }
            },
            created: this.serviceCreated,
            stopped: this.serviceStopped
        });
    }
    async request(ctx) {
        // Format URI
        let uri = "https://oauth.reddit.com";
        if (!ctx.params.endpoint.startsWith("/")) {
            uri += "/" + ctx.params.endpoint;
        }
        else {
            uri += ctx.params.endpoint;
        }
        // Retrieve the correct access token
        let token;
        if (ctx.meta.serviceType && ctx.meta.userId) {
            token = await ctx.call("v4.accounts.linked.get.access", {
                serviceType: ctx.meta.serviceType,
                userId: ctx.meta.userId,
                requestedServiceType: "reddit"
            });
        }
        else {
            token = await ctx.call("v4.accounts.default.get.access", {
                serviceType: "reddit"
            });
        }
        try {
            return await this.rateLimiter.schedule(() => ctx.call("v4.api.core.request", {
                uri,
                method: ctx.params.method,
                headers: {
                    "User-Agent": this.settings.reddit.oauth.userAgent,
                    Authorization: `Bearer ${token}`,
                    Accept: "application/json"
                },
                qs: ctx.params.qs || undefined,
                form: ctx.params.form || undefined,
                json: true
            }));
        }
        catch (err) {
            if (err.statusCode === 401) {
                // Clear access token cache
                ctx.emit("v4.accounts.reddit.cache.clear");
                throw new moleculer_1.Errors.MoleculerRetryableError(err.message, err.statusCode, "ACCESS_TOKEN_EXPIRED");
            }
            else if (err.statusCode >= 400 && err.statusCode < 500) {
                throw new moleculer_1.Errors.MoleculerClientError(err.message, err.statusCode, "REDDIT_CLIENT_ERROR", {
                    uri: err.options.uri,
                    method: err.options.method
                });
            }
            else if (err.statusCode >= 500) {
                throw new moleculer_1.Errors.MoleculerRetryableError(err.message, err.statusCode, "REDDIT_SERVER_ERROR", {
                    uri: err.options.uri,
                    method: err.options.method
                });
            }
            else {
                throw new moleculer_1.Errors.MoleculerError(err.message, err.statusCode, "REDDIT_UNKNOWN_ERROR", {
                    uri: err.options.uri,
                    method: err.options.method
                });
            }
        }
    }
    serviceCreated() {
        // Terminate if any of the required settings aren't supplied
        if (!this.settings.bottleneck.redis.host)
            this.broker.fatal("Redis host not supplied! Make sure API_REDDIT_REDIS_HOST is populated!");
        if (!this.settings.reddit.oauth.userAgent)
            this.broker.fatal("Reddit user agent not supplied! Make sure API_REDDIT_OAUTH_USERAGENT is populated!");
        // Set up and configure Reddit rate limiter
        this.rateLimiter = new bottleneck_1.default({
            id: `${this.broker.nodeID}/api.reddit`,
            // Reddit API limits
            reservoir: 60,
            reservoirRefreshAmount: 60,
            reservoirRefreshInterval: 60 * 1000,
            maxConcurrent: 1,
            minTime: 500,
            datastore: "ioredis",
            clearDatastore: true,
            clientOptions: {
                host: this.settings.bottleneck.redis.host,
                port: this.settings.bottleneck.redis.port
            }
        });
    }
    async serviceStopped() {
        await this.rateLimiter.disconnect();
    }
}
exports.default = RedditCoreService;
