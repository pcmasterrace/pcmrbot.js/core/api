"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const moleculer_1 = require("moleculer");
const moment_timezone_1 = tslib_1.__importDefault(require("moment-timezone"));
class RedditWikiService extends moleculer_1.Service {
    constructor(broker) {
        super(broker);
        this.parseServiceSchema({
            name: "api.reddit.wiki",
            version: 4,
            dependencies: [
                { name: "api.reddit.core", version: 4 }
            ],
            actions: {
                getPage: {
                    name: "page.get",
                    params: {
                        subreddit: {
                            type: "string",
                            empty: false,
                            convert: true,
                            trim: true
                        },
                        page: {
                            type: "string",
                            empty: false,
                            convert: true,
                            trim: true
                        }
                    },
                    handler: this.getPage
                },
                setPage: {
                    name: "page.set",
                    params: {
                        subreddit: {
                            type: "string",
                            empty: false,
                            convert: true,
                            trim: true
                        },
                        page: {
                            type: "string",
                            empty: false,
                            convert: true,
                            trim: true
                        },
                        body: "string",
                        reason: {
                            type: "string",
                            optional: true
                        },
                        previousRevision: {
                            type: "string",
                            optional: true
                        }
                    },
                    handler: this.setPage
                }
            }
        });
    }
    /**
     * Gets a page from Reddit
     * @static
     * @function
     * @name page.get
     * @param {string} subreddit - The subreddit the page is on
     * @param {string} page - The page to retrieve
     * @returns {Typings.Reddit.WikiPage} Returns a WikiPage object
     */
    async getPage(ctx) {
        const redditResponse = await ctx.call("v4.api.reddit.core.request", {
            endpoint: `/r/${ctx.params.subreddit}/wiki/${ctx.params.page}`,
            method: "GET"
        });
        const response = {
            body: redditResponse.data.content_md,
            mayRevise: redditResponse.data.may_revise,
            revision: {
                by: redditResponse.data.revision_by,
                date: moment_timezone_1.default.utc(redditResponse.data.revision_date).toDate(),
                id: redditResponse.data.revision_id,
                reason: redditResponse.data.reason
            }
        };
        return response;
    }
    /**
     * Creates or updates a page on Reddit
     * @static
     * @function
     * @name page.set
     * @param {string} subreddit - The subreddit the page is on
     * @param {string} page - The page to retrieve
     * @param {string} body - The new page body
     * @param {string} [reason] - The page update reason
     * @param {string} [previousRevision] - The previous page revision to append the update to. Defaults to the most recent revision if left undefined
     * @returns {object}
     */
    async setPage(ctx) {
        const response = await ctx.call("v4.api.reddit.core.request", {
            endpoint: `/r/${ctx.params.subreddit}/api/wiki/edit`,
            method: "POST",
            form: {
                content: ctx.params.body,
                page: ctx.params.page,
                previous: ctx.params.previousRevision || undefined,
                reason: ctx.params.reason || undefined
            }
        });
        return {
            edited: true
        };
    }
}
exports.default = RedditWikiService;
