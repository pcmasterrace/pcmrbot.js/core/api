"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const moleculer_1 = require("moleculer");
const moment_timezone_1 = tslib_1.__importDefault(require("moment-timezone"));
class SubredditService extends moleculer_1.Service {
    constructor(broker) {
        super(broker);
        this.parseServiceSchema({
            name: "api.reddit.subreddit",
            version: 4,
            dependencies: [
                { name: "api.reddit.post", version: 4 },
                { name: "api.reddit.core", version: 4 }
            ],
            actions: {
                getHot: {
                    name: "index.hot",
                    params: {
                        subreddit: {
                            type: "string",
                            empty: false,
                            trim: true,
                            lowercase: true,
                            convert: true
                        },
                        after: {
                            type: "string",
                            empty: false,
                            trim: true,
                            lowercase: true,
                            optional: true
                        },
                        before: {
                            type: "string",
                            empty: false,
                            trim: true,
                            lowercase: true,
                            optional: true
                        },
                        count: {
                            type: "number",
                            integer: true,
                            positive: true,
                            convert: true,
                            optional: true
                        },
                        limit: {
                            type: "number",
                            integer: true,
                            positive: true,
                            convert: true,
                            optional: true
                        }
                    },
                    handler: this.getHot
                },
                getNew: {
                    name: "index.new",
                    params: {
                        subreddit: {
                            type: "string",
                            empty: false,
                            trim: true,
                            lowercase: true,
                            convert: true
                        },
                        after: {
                            type: "string",
                            empty: false,
                            trim: true,
                            lowercase: true,
                            optional: true
                        },
                        before: {
                            type: "string",
                            empty: false,
                            trim: true,
                            lowercase: true,
                            optional: true
                        },
                        count: {
                            type: "number",
                            integer: true,
                            positive: true,
                            convert: true,
                            optional: true
                        },
                        limit: {
                            type: "number",
                            integer: true,
                            positive: true,
                            convert: true,
                            optional: true
                        }
                    },
                    handler: this.getNew
                },
                getRising: {
                    name: "index.rising",
                    params: {
                        subreddit: {
                            type: "string",
                            empty: false,
                            trim: true,
                            lowercase: true,
                            convert: true
                        },
                        after: {
                            type: "string",
                            empty: false,
                            trim: true,
                            lowercase: true,
                            optional: true
                        },
                        before: {
                            type: "string",
                            empty: false,
                            trim: true,
                            lowercase: true,
                            optional: true
                        },
                        count: {
                            type: "number",
                            integer: true,
                            positive: true,
                            convert: true,
                            optional: true
                        },
                        limit: {
                            type: "number",
                            integer: true,
                            positive: true,
                            convert: true,
                            optional: true
                        }
                    },
                    handler: this.getRising
                },
                getControversial: {
                    name: "index.controversial",
                    params: {
                        subreddit: {
                            type: "string",
                            empty: false,
                            trim: true,
                            lowercase: true,
                            convert: true
                        },
                        after: {
                            type: "string",
                            empty: false,
                            trim: true,
                            lowercase: true,
                            optional: true
                        },
                        before: {
                            type: "string",
                            empty: false,
                            trim: true,
                            lowercase: true,
                            optional: true
                        },
                        count: {
                            type: "number",
                            integer: true,
                            positive: true,
                            convert: true,
                            optional: true
                        },
                        limit: {
                            type: "number",
                            integer: true,
                            positive: true,
                            convert: true,
                            optional: true
                        },
                        time: {
                            type: "enum",
                            values: ["hour", "day", "week", "month", "year", "all"],
                            optional: true
                        }
                    },
                    handler: this.getControversial
                },
                getTop: {
                    name: "index.top",
                    params: {
                        subreddit: {
                            type: "string",
                            empty: false,
                            trim: true,
                            lowercase: true,
                            convert: true
                        },
                        after: {
                            type: "string",
                            empty: false,
                            trim: true,
                            lowercase: true,
                            optional: true
                        },
                        before: {
                            type: "string",
                            empty: false,
                            trim: true,
                            lowercase: true,
                            optional: true
                        },
                        count: {
                            type: "number",
                            integer: true,
                            positive: true,
                            convert: true,
                            optional: true
                        },
                        limit: {
                            type: "number",
                            integer: true,
                            positive: true,
                            convert: true,
                            optional: true
                        },
                        time: {
                            type: "enum",
                            values: ["hour", "day", "week", "month", "year", "all"],
                            optional: true
                        }
                    },
                    handler: this.getTop
                },
                getComments: {
                    name: "comments",
                    params: {
                        subreddit: {
                            type: "string",
                            empty: false,
                            trim: true,
                            lowercase: true,
                            convert: true
                        },
                        after: {
                            type: "string",
                            empty: false,
                            trim: true,
                            lowercase: true,
                            optional: true
                        },
                        before: {
                            type: "string",
                            empty: false,
                            trim: true,
                            lowercase: true,
                            optional: true
                        },
                        count: {
                            type: "number",
                            integer: true,
                            positive: true,
                            convert: true,
                            optional: true
                        },
                        limit: {
                            type: "number",
                            integer: true,
                            positive: true,
                            convert: true,
                            optional: true
                        }
                    },
                    handler: this.getComments
                },
                getAbout: {
                    name: "about",
                    params: {
                        subreddit: {
                            type: "string",
                            empty: false,
                            trim: true,
                            lowercase: true,
                            convert: true
                        }
                    },
                    handler: this.getAbout
                }
            }
        });
    }
    /**
     * Retrieves the content of the specified subreddit's /hot index.
     * @static
     * @function
     * @name index.hot
     * @param {string} subreddit - The subreddit to fetch
     * @param {string} [after] - The item to start retrieving from. All retrieved items will be **older** than this
     * @param {string} [before] - The item to retrieve up to. All retrieved items will be **newer** than this
     * @param {number} [limit] - The maximum number of items to return in this slice of the listing
     * @param {number} [count] - The number of items already seen in the listing
     * @returns {Reddit.SubredditIndex} Returns an array of Post objects
     */
    async getHot(ctx) {
        let redditResponse = await ctx.call("v4.api.reddit.core.request", {
            endpoint: `/r/${ctx.params.subreddit}/hot`,
            method: "GET",
            qs: {
                after: ctx.params.after || undefined,
                before: ctx.params.before || undefined,
                count: ctx.params.count || undefined,
                limit: ctx.params.limit || undefined
            }
        });
        let response = [];
        for (let thing of redditResponse.data.children) {
            response.push(await ctx.call("v4.api.reddit.post.sanitize.t3", thing));
        }
        return response;
    }
    /**
     * Retrieves the content of the specified subreddit's /new index.
     * @static
     * @function
     * @name index.new
     * @param {string} subreddit - The subreddit to fetch
     * @param {string} [after] - The item to start retrieving from. All retrieved items will be **older** than this
     * @param {string} [before] - The item to retrieve up to. All retrieved items will be **newer** than this
     * @param {number} [limit] - The maximum number of items to return in this slice of the listing
     * @param {number} [count] - The number of items already seen in the listing
     * @returns {Reddit.SubredditIndex} Returns an array of Post objects
     */
    async getNew(ctx) {
        let redditResponse = await ctx.call("v4.api.reddit.core.request", {
            endpoint: `/r/${ctx.params.subreddit}/new`,
            method: "GET",
            qs: {
                after: ctx.params.after || undefined,
                before: ctx.params.before || undefined,
                count: ctx.params.count || undefined,
                limit: ctx.params.limit || undefined
            }
        });
        let response = [];
        for (let thing of redditResponse.data.children) {
            response.push(await ctx.call("v4.api.reddit.post.sanitize.t3", thing));
        }
        return response;
    }
    /**
     * Retrieves the content of the specified subreddit's /rising index.
     * @static
     * @function
     * @name index.rising
     * @param {string} subreddit - The subreddit to fetch
     * @param {string} [after] - The item to start retrieving from. All retrieved items will be **older** than this
     * @param {string} [before] - The item to retrieve up to. All retrieved items will be **newer** than this
     * @param {number} [limit] - The maximum number of items to return in this slice of the listing
     * @param {number} [count] - The number of items already seen in the listing
     * @returns {Reddit.SubredditIndex} Returns an array of Post objects
     */
    async getRising(ctx) {
        let redditResponse = await ctx.call("v4.api.reddit.core.request", {
            endpoint: `/r/${ctx.params.subreddit}/rising`,
            method: "GET",
            qs: {
                after: ctx.params.after || undefined,
                before: ctx.params.before || undefined,
                count: ctx.params.count || undefined,
                limit: ctx.params.limit || undefined
            }
        });
        let response = [];
        for (let thing of redditResponse.data.children) {
            response.push(await ctx.call("v4.api.reddit.post.sanitize.t3", thing));
        }
        return response;
    }
    /**
     * Retrieves the content of the specified subreddit's /controversial index.
     * @static
     * @function
     * @name index.controversial
     * @param {string} subreddit - The subreddit to fetch
     * @param {string} [after] - The item to start retrieving from. All retrieved items will be **older** than this
     * @param {string} [before] - The item to retrieve up to. All retrieved items will be **newer** than this
     * @param {number} [limit] - The maximum number of items to return in this slice of the listing
     * @param {number} [count] - The number of items already seen in the listing
     * @param {"hour" | "day" | "week" | "month" | "year" | "all"} time - The time period to retrieve
     * @returns {Reddit.SubredditIndex} Returns an array of Post objects
     */
    async getControversial(ctx) {
        let redditResponse = await ctx.call("v4.api.reddit.core.request", {
            endpoint: `/r/${ctx.params.subreddit}/controversial`,
            method: "GET",
            qs: {
                after: ctx.params.after || undefined,
                before: ctx.params.before || undefined,
                count: ctx.params.count || undefined,
                limit: ctx.params.limit || undefined,
                t: ctx.params.time || undefined
            }
        });
        let response = [];
        for (let thing of redditResponse.data.children) {
            response.push(await ctx.call("v4.api.reddit.post.sanitize.t3", thing));
        }
        return response;
    }
    /**
     * Retrieves the content of the specified subreddit's /top index.
     * @static
     * @function
     * @name index.top
     * @param {string} subreddit - The subreddit to fetch
     * @param {string} [after] - The item to start retrieving from. All retrieved items will be **older** than this
     * @param {string} [before] - The item to retrieve up to. All retrieved items will be **newer** than this
     * @param {number} [limit] - The maximum number of items to return in this slice of the listing
     * @param {number} [count] - The number of items already seen in the listing
     * @param {"hour" | "day" | "week" | "month" | "year" | "all"} time - The time period to retrieve
     * @returns {Reddit.SubredditIndex} Returns an array of Post objects
     */
    async getTop(ctx) {
        let redditResponse = await ctx.call("v4.api.reddit.core.request", {
            endpoint: `/r/${ctx.params.subreddit}/top`,
            method: "GET",
            qs: {
                after: ctx.params.after || undefined,
                before: ctx.params.before || undefined,
                count: ctx.params.count || undefined,
                limit: ctx.params.limit || undefined,
                t: ctx.params.time || undefined
            }
        });
        let response = [];
        for (let thing of redditResponse.data.children) {
            response.push(await ctx.call("v4.api.reddit.post.sanitize.t3", thing));
        }
        return response;
    }
    /**
     * Retrieves the content of the specified subreddit's /comments index.
     * @static
     * @function
     * @name comments
     * @param {string} subreddit - The subreddit to fetch
     * @param {string} [after] - The item to start retrieving from. All retrieved items will be **older** than this
     * @param {string} [before] - The item to retrieve up to. All retrieved items will be **newer** than this
     * @param {number} [limit] - The maximum number of items to return in this slice of the listing
     * @param {number} [count] - The number of items already seen in the listing
     * @returns {Reddit.Comment[]} Returns an array of Post objects
     */
    async getComments(ctx) {
        let redditResponse = await ctx.call("v4.api.reddit.core.request", {
            endpoint: `/r/${ctx.params.subreddit}/comments`,
            method: "GET",
            qs: {
                after: ctx.params.after || undefined,
                before: ctx.params.before || undefined,
                count: ctx.params.count || undefined,
                limit: ctx.params.limit || undefined
            }
        });
        let result = [];
        for (let thing of redditResponse.data.children) {
            result.push(await ctx.call("v4.api.reddit.post.sanitize.t1", thing));
        }
        return result;
    }
    /**
     * Retrieves the subreddit information and formats it for proper consumption.
     * @static
     * @function
     * @name about
     * @param {string} subreddit - The subreddit to retrieve information for
     * @returns {Reddit.Subreddit} Returns a Subreddit object
     */
    async getAbout(ctx) {
        let redditResponse = await ctx.call("v4.api.reddit.core.request", {
            endpoint: `/r/${ctx.params.subreddit}/about`,
            method: "GET"
        });
        const result = {
            activeUsers: redditResponse.data.active_user_count,
            advertiserCategory: redditResponse.data.advertiser_category,
            allOriginalContent: redditResponse.data.all_original_content,
            allowDiscovery: redditResponse.data.allow_discovery,
            allowImages: redditResponse.data.allow_images,
            allowVideoGifs: redditResponse.data.allow_videogifs,
            allowVideos: redditResponse.data.allow_videos,
            banner: {
                background: {
                    color: redditResponse.data.banner_background_color,
                    image: redditResponse.data.banner_background_image
                },
                height: redditResponse.data.banner_size ? redditResponse.data.banner_size[1] : null,
                image: redditResponse.data.banner_img,
                mobileImage: redditResponse.data.mobile_banner_image,
                width: redditResponse.data.banner_size ? redditResponse.data.banner_size[0] : null
            },
            canAssignLinkFlair: redditResponse.data.can_assign_link_flair,
            canAssignUserFlair: redditResponse.data.can_assign_user_flair,
            collapseDeletedComments: redditResponse.data.collapse_deleted_comments,
            collectionsEnabled: redditResponse.data.collections_enabled,
            commentScoreHideMins: redditResponse.data.comment_score_hide_mins,
            communityIcon: redditResponse.data.community_icon,
            created: moment_timezone_1.default.utc(redditResponse.data.created_utc).toDate(),
            description: redditResponse.data.description,
            disableContributorRequests: redditResponse.data.disable_contributor_requests,
            displayName: redditResponse.data.display_name,
            emojis: {
                enabled: redditResponse.data.emojis_enabled,
                height: redditResponse.data.emojis_custom_size ? redditResponse.data.emojis_custom_size[1] : null,
                width: redditResponse.data.emojis_custom_size ? redditResponse.data.emojis_custom_size[0] : null
            },
            eventPostsEnabled: redditResponse.data.event_posts_enabled,
            freeformReports: redditResponse.data.free_form_reports,
            hasMenuWidget: redditResponse.data.has_menu_widget,
            header: {
                height: redditResponse.data.header_size ? redditResponse.data.header_size[1] : null,
                image: redditResponse.data.header_img,
                title: redditResponse.data.header_title,
                width: redditResponse.data.header_size ? redditResponse.data.header_size[0] : null
            },
            icon: {
                height: redditResponse.data.icon_size ? redditResponse.data.icon_size[1] : null,
                image: redditResponse.data.icon_img,
                width: redditResponse.data.icon_size ? redditResponse.data.icon_size[0] : null
            },
            isCrosspostable: redditResponse.data.is_crosspostable_subreddit,
            isEnrolledInNewModmail: redditResponse.data.is_enrolled_in_new_modmail,
            keyColor: redditResponse.data.key_color,
            lang: redditResponse.data.lang,
            linkFlair: {
                enabled: redditResponse.data.link_flair_enabled,
                position: redditResponse.data.link_flair_position
            },
            name: redditResponse.data.name,
            nsfw: redditResponse.data.over18,
            originalContentTagEnabled: redditResponse.data.original_content_tag_enabled,
            primaryColor: redditResponse.data.primary_color,
            publicDescription: redditResponse.data.public_description,
            publicTraffic: redditResponse.data.public_traffic,
            quarantine: redditResponse.data.quarantine,
            restrictCommenting: redditResponse.data.restrict_commenting,
            restrictPosting: redditResponse.data.restrict_posting,
            spoilersEnabled: redditResponse.data.spoilers_enabled,
            submissionType: redditResponse.data.submission_type,
            submitLinkLabel: redditResponse.data.submit_link_label,
            submitText: redditResponse.data.submit_text,
            submitTextLabel: redditResponse.data.submit_text_label,
            subredditType: redditResponse.data.subreddit_type,
            subscribers: redditResponse.data.subscribers,
            suggestedCommentSort: redditResponse.data.suggested_comment_sort,
            title: redditResponse.data.title,
            url: redditResponse.data.url,
            wikiEnabled: redditResponse.data.wiki_enabled
        };
        return result;
    }
}
exports.default = SubredditService;
