import { Service, Context } from "moleculer";
import { Typings } from "@pcmrbotjs/core-typings";
export default class RedditWikiService extends Service {
    constructor(broker: any);
    /**
     * Gets a page from Reddit
     * @static
     * @function
     * @name page.get
     * @param {string} subreddit - The subreddit the page is on
     * @param {string} page - The page to retrieve
     * @returns {Typings.Reddit.WikiPage} Returns a WikiPage object
     */
    getPage(ctx: Context<{
        subreddit: string;
        page: string;
    }, any>): Promise<Typings.Reddit.WikiPage>;
    /**
     * Creates or updates a page on Reddit
     * @static
     * @function
     * @name page.set
     * @param {string} subreddit - The subreddit the page is on
     * @param {string} page - The page to retrieve
     * @param {string} body - The new page body
     * @param {string} [reason] - The page update reason
     * @param {string} [previousRevision] - The previous page revision to append the update to. Defaults to the most recent revision if left undefined
     * @returns {object}
     */
    setPage(ctx: Context<{
        subreddit: string;
        page: string;
        body: string;
        reason?: string;
        previousRevision?: string;
    }, any>): Promise<{
        edited: boolean;
    }>;
}
