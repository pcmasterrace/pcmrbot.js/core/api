import { Service, Context } from "moleculer";
import { Typings } from '@pcmrbotjs/core-typings';
export default class RedditToolboxService extends Service {
    constructor(broker: any);
    /**
     * Gets and unescapes the Toolbox config from Reddit.
     * @static
     * @function
     * @name config
     * @param {string} subreddit - The subreddit to retrieve the config from
     * @returns {Typings.Reddit.ToolboxConfig} Returns a ToolboxConfig object
     */
    getConfig(ctx: Context<{
        subreddit: string;
    }, any>): Promise<Typings.Reddit.ToolboxConfig>;
    /**
     * Renders a removal message.
     * @static
     * @function
     * @name config.render
     * @param {string} subreddit - The subreddit to retrieve the config from
     * @param {string[]} [reasons=[]] - The reasons to render in the post
     * @param {string} [customReason] - A custom reason to add
     * @param {boolean} [useHeader=true] - Whether to render the reason header
     * @param {boolean} [useFooter=true] - Whether to render the reason footer
     * @param {string} [author] - The value to replace {{author}} tags with
     * @param {string} [kind] - The value to replace {{kind}} tags with
     * @param {string} [mod] - The value to replace {{mod}} tags with
     * @param {string} [title] - The value to replace {{title}} tags with
     * @param {string} [url] - The value to replace {{url}} tags with
     * @param {string} [domain] - The value to replace {{domain}} tags with
     * @param {string} [link] - The value to replace {{link}} tags with
     */
    renderRemovalMessage(ctx: Context<{
        subreddit: string;
        reasons: string[];
        customReason?: string;
        useHeader?: boolean;
        useFooter?: boolean;
        author?: string;
        kind?: string;
        mod?: string;
        title?: string;
        url?: string;
        domain?: string;
        link?: string;
    }, any>): Promise<string>;
}
