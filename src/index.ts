import { ServiceBroker } from "moleculer";

// Core
import ApiCoreService from "./services/core";

// Reddit
import RedditCommentService from "./services/reddit/comment";
import RedditCoreService from "./services/reddit/core";
const RedditPostService = require( "./services/reddit/post");
import RedditToolboxService from "./services/reddit/toolbox";
import RedditSubredditService from "./services/reddit/subreddit";
import RedditSubmissionService from "./services/reddit/submission";
import RedditUserService from "./services/reddit/user";
import RedditWikiService from "./services/reddit/wiki";

// Reddit RT
import RedditRtPostService from "./services/reddit/rt/post";

// Slack 
import SlackCoreService from "./services/slack/core";
import SlackWebService from "./services/slack/web";
import SlackUtilitiesService from "./services/slack/utilities";

export default function registerAllApiServices(broker: ServiceBroker): void {
    broker.createService(ApiCoreService);

    broker.createService(RedditCommentService);
    broker.createService(RedditCoreService);
    broker.createService(RedditPostService);
    broker.createService(RedditToolboxService);
    broker.createService(RedditSubredditService);
    broker.createService(RedditSubmissionService);
    broker.createService(RedditUserService);
    broker.createService(RedditWikiService);

    broker.createService(RedditRtPostService);

    broker.createService(SlackCoreService);
    broker.createService(SlackWebService);
    broker.createService(SlackUtilitiesService);
}

export { 
    ApiCoreService,

    RedditCommentService,
    RedditCoreService,
    RedditPostService,
    RedditToolboxService,
    RedditSubredditService,
    RedditSubmissionService,
    RedditUserService,
    RedditWikiService,

    RedditRtPostService,
    
    SlackCoreService,
    SlackWebService,
    SlackUtilitiesService,
}