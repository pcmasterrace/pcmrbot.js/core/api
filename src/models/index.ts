import GlobalSequelize from "@pcmrbotjs/global-sequelize";
import { Model } from "sequelize";

import { ApiRedditRtSeenPost, ApiRedditRtSeenPostAttributes, ApiRedditRtSeenPostOptions } from "./reddit/rt/seenPost";

export default function initDb(connectionString: string): GlobalSequelize {
    const gs = new GlobalSequelize(connectionString);

    const registerRedditRtSeenPost = gs.models.ApiRedditRtSeenPost === undefined;

    // Initialize accounts
    if (registerRedditRtSeenPost) {
        ApiRedditRtSeenPost.init(ApiRedditRtSeenPostAttributes, ApiRedditRtSeenPostOptions(gs.db));
        gs.models.ApiRedditRtSeenPost = ApiRedditRtSeenPost;
    }

    // Establish associations


    // Synchronize models
    if (registerRedditRtSeenPost) {
        gs.models.ApiRedditRtSeenPost.sync({alter: true});
    }

    return gs;
}

export interface ModelsInterface {
    ApiRedditRtSeenPost?: typeof ApiRedditRtSeenPost,
    [key: string]: typeof Model;
}

export {
    ApiRedditRtSeenPost
}