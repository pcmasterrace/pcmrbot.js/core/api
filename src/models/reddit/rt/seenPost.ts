import { Sequelize, Model, DataTypes, ModelAttributes, InitOptions } from "sequelize";

export class ApiRedditRtSeenPost extends Model {
    public id!: string;
    public firstSeen!: Date;
}

export const ApiRedditRtSeenPostAttributes: ModelAttributes = {
    id: {
        type: DataTypes.STRING,
        allowNull: false,
        primaryKey: true,
        unique: true
    },
    firstSeen: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: () => new Date()
    }
}

export function ApiRedditRtSeenPostOptions(sequelize: Sequelize): InitOptions {
    return {
        sequelize,
        tableName: "api_reddit_rt_SeenPost"
    }
}