import { Service, Context, Errors } from "moleculer";

const RedditPostService = require("./post");

import { Typings } from "@pcmrbotjs/core-typings";

export default class RedditSubmissionService extends Service {
    constructor(broker) {
        super(broker);

        this.parseServiceSchema({
            name: "api.reddit.submission",
            version: 4,
            mixins: [ RedditPostService ]
        });
    }
}