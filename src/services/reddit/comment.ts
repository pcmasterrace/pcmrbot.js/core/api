import { Service, Context, Errors } from "moleculer";

const RedditPostService = require("./post");

export default class RedditCommentService extends Service {
    constructor(broker) {
        super(broker);

        this.parseServiceSchema({
            name: "api.reddit.comment",
            version: 4,
            mixins: [ RedditPostService ]
        })
    }
}