import { Service, Context, Errors } from "moleculer";
import { Sequelize, Op } from "sequelize";

const ApiDatabase = require("../../database");
import { ModelsInterface, ApiRedditRtSeenPost } from "../../../models/index";

import { Typings } from "@pcmrbotjs/core-typings";

export default class RedditRtPostService extends Service {
    public db: Sequelize;
    public models: ModelsInterface;

    constructor(broker) {
        super(broker);

        this.parseServiceSchema({
            name: "api.reddit.rt.post",
            version: 4,
            mixins: [ ApiDatabase ],
            dependencies: [
                { name: "api.reddit.subreddit", version: 4 }
            ],
            settings: {
                subreddit: process.env.API_REDDIT_RT_SUBREDDIT,
                interval: Number(process.env.API_REDDIT_RT_INTERVAL) * 1000 || 10000
            },
            actions: {
                pollReddit: {
                    name: "poll",
                    params: {
                        subreddit: "string"
                    },
                    handler: this.pollReddit
                }
            },
            created: this.serviceCreated,
            started: this.serviceStarted
        });
    }

    async pollReddit(ctx: Context<{
        subreddit: string
    }, any>) {
        // Fetch new posts from Reddit
        const newSubmissions: Typings.Reddit.SubredditIndex = await ctx.call("v4.api.reddit.subreddit.index.new", {
            subreddit: ctx.params.subreddit,
            limit: 100
        });
        
        const newComments: Typings.Reddit.Comment[] = await ctx.call("v4.api.reddit.subreddit.comments", {
            subreddit: ctx.params.subreddit,
            limit: 100
        });

        // Async for reasons
        newSubmissions.map(async submission => {
            const [name, created]: [ApiRedditRtSeenPost, boolean] = await this.models.ApiRedditRtSeenPost.findOrCreate({where: {
                id: submission.name
            }});
            if (created) {
                ctx.emit("v4.api.reddit.rt.post.submission", submission);
            }
        });

        newComments.map(async comment => {
            const [name, created]: [ApiRedditRtSeenPost, boolean] = await this.models.ApiRedditRtSeenPost.findOrCreate({where: {
                id: comment.name
            }});
            if (created) {
                ctx.emit("v4.api.reddit.rt.post.comment", comment);
            }
        });
    }

    serviceCreated() {
        if(!this.settings.subreddit) this.broker.fatal("Reddit RT subreddit not supplied! Make sure API_REDDIT_RT_SUBREDDIT is populated!");
    }

    async serviceStarted() {
        setInterval(() => this.broker.call("v4.api.reddit.rt.post.poll", {
            subreddit: this.settings.subreddit
        }), this.settings.interval);
    }
}