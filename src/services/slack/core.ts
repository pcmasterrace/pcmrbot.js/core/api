import { Service, Context, Errors } from "moleculer";
import Bottleneck from "bottleneck";
import os from "os";

export default class SlackCoreService extends Service {
    public rateLimiter: {
        t1: Bottleneck,
        t2: Bottleneck,
        t3: Bottleneck,
        t4: Bottleneck,
        message: Bottleneck
    };

    constructor(broker) {
        super(broker);

        this.parseServiceSchema({
            name: "api.slack.core",
            version: 4,
            dependencies: [
                { name: "api.core", version: 4 }
            ],
            settings: {
                $secureSettings: ["slack"],
                bottleneck: {
                    redis: {
                        host: process.env.API_SLACK_REDIS_HOST,
                        port: Number(process.env.API_SLACK_REDIS_PORT) || 6379
                    }
                },
                slack: {
                    token: process.env.API_SLACK_TOKEN
                }
            },
            actions: {
                request: {
                    name: "request",
                    params: {
                        endpoint: {
                            type: "string",
                            empty: false,
                            trim: true,
                            convert: true
                        },
                        method: {
                            type: "string",
                            convert: true
                        },
                        qs: {
                            type: "any",
                            optional: true
                        },
                        form: {
                            type: "object",
                            optional: true
                        },
                        json: {
                            type: "object",
                            optional: true
                        },
                        tier: {
                            type: "enum",
                            values: ["t1", "t2", "t3", "t4", "message"]
                        }
                    },
                    handler: this.request
                },
                testApi: {
                    name: "api.test",
                    handler: this.testApi
                }
            },
            created: this.serviceCreated,
            stopped: this.serviceStopped
        });
    }

    async testApi(ctx: Context<{}, any>) {
        return ctx.call("v4.api.slack.core.request", {
            endpoint: "api.test",
            method: "POST",
            tier: "t4"
        });
    }

    async request(ctx: Context<{
        endpoint: string,
        method: string,
        qs?: any,
        form?: any,
        json?: any,
        tier: "t1" | "t2" | "t3" | "t4" | "message"
    }, any>) {
        // Format URI
        let uri = "https://slack.com/api";
        if (!ctx.params.endpoint.startsWith("/")) {
            uri += "/" + ctx.params.endpoint;
        } else {
            uri += ctx.params.endpoint;
        }

        const request = {
            uri,
            method: ctx.params.method,
            headers: {
                "User-Agent": `PCMRBot.js v${this.version} node/${process.version.replace("v", "")} ${os.platform()}/${os.release()}`,
                Authorization: `Bearer ${this.settings.slack.token}`,
                charset: "utf-8"
            },
            qs: ctx.params.qs || undefined,
            form: ctx.params.form || undefined,
            json: ctx.params.json || true
        };

        try {
            switch(ctx.params.tier) {
                case "t1":
                    return await this.rateLimiter.t1.schedule(() => ctx.call("v4.api.core.request", request));
                case "t2": 
                    return await this.rateLimiter.t2.schedule(() => ctx.call("v4.api.core.request", request));
                case "t3": 
                    return await this.rateLimiter.t3.schedule(() => ctx.call("v4.api.core.request", request));
                case "t4": 
                    return await this.rateLimiter.t4.schedule(() => ctx.call("v4.api.core.request", request));
                case "message": 
                    return await this.rateLimiter.message.schedule(() => ctx.call("v4.api.core.request", request));
                default: 
                    throw new Errors.MoleculerError("How did you get here?", 401, "INVALID_TYPE");
            }
        } catch (err) {
            if (err.statusCode >= 400 && err.statusCode < 500) {
                throw new Errors.MoleculerClientError(err.message, err.statusCode, "SLACK_CLIENT_ERROR", {
                    uri: err.options.uri,
                    method: err.options.method
                });
            } else if (err.statusCode >= 500) {
                throw new Errors.MoleculerRetryableError(err.message, err.statusCode, "SLACK_SERVER_ERROR", {
                    uri: err.options.uri,
                    method: err.options.method
                });
            } else {
                throw new Errors.MoleculerError(err.message, err.statusCode, "SLACK_UNKNOWN_ERROR", {
                    uri: err.options.uri,
                    method: err.options.method
                });
            }
        }
    }

    serviceCreated() {
        // Terminate if any of the required settings aren't supplied
        if (!this.settings.bottleneck.redis.host) this.broker.fatal("Redis host not supplied! Make sure API_SLACK_REDIS_HOST is populated!");
        if (!this.settings.slack.token) this.broker.fatal("Slack token not supplied! Make sure API_SLACK_TOKEN is populated!");

        const connection = new Bottleneck.IORedisConnection({
            clientOptions: {
                host: this.settings.bottleneck.redis.host,
                port: this.settings.bottleneck.redis.port
            }
        });

        this.rateLimiter = {
            t1: new Bottleneck({
                id: `pcmrbot.js/api.slack.t1`,
    
                // Slack tier 1 limits
                reservoir: 1,
                reservoirRefreshAmount: 1,
                reservoirRefreshInterval: 60 * 1000,
    
                maxConcurrent: 1,
                minTime: 500,
    
                datastore: "ioredis",
                clearDatastore: true,
                connection
            }),
            t2: new Bottleneck({
                id: `pcmrbot.js/api.slack.t2`,
    
                // Slack tier 2 limits
                reservoir: 20,
                reservoirRefreshAmount: 20,
                reservoirRefreshInterval: 60 * 1000,
    
                maxConcurrent: 1,
                minTime: 1500,
    
                datastore: "ioredis",
                clearDatastore: true,
                connection
            }),
            t3: new Bottleneck({
                id: `pcmrbot.js/api.slack.t3`,
    
                // Slack tier 3 limits
                reservoir: 50,
                reservoirRefreshAmount: 50,
                reservoirRefreshInterval: 60 * 1000,
    
                maxConcurrent: 1,
                minTime: 500,
    
                datastore: "ioredis",
                clearDatastore: true,
                connection
            }),
            t4: new Bottleneck({
                id: `pcmrbot.js/api.slack.t4`,
    
                // Slack tier 4 limits
                reservoir: 100,
                reservoirRefreshAmount: 100,
                reservoirRefreshInterval: 60 * 1000,
    
                maxConcurrent: 1,
                minTime: 500,
    
                datastore: "ioredis",
                clearDatastore: true,
                connection
            }),
            message: new Bottleneck({
                id: `pcmrbot.js/api.slack.message`,
    
                // Slack message posting limits
                reservoir: 60,
                reservoirRefreshAmount: 60,
                reservoirRefreshInterval: 60 * 1000,
    
                maxConcurrent: 1,
                minTime: 500,
    
                datastore: "ioredis",
                clearDatastore: true,
                connection
            })
        };
    }

    async serviceStopped() {
        await Promise.all([
            this.rateLimiter.t1.disconnect(),
            this.rateLimiter.t2.disconnect(),
            this.rateLimiter.t3.disconnect(),
            this.rateLimiter.t4.disconnect(),
            this.rateLimiter.message.disconnect()
        ]);
    }
}