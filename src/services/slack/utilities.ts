import { Service, Context, Errors } from "moleculer";
import snuownd from "snuownd";
// This apparently doesn't play nice with tslib
const htmlToMrkdwn = require("html-to-mrkdwn");

export default class SlackUtilitiesService extends Service {
    constructor(broker) {
        super(broker);

        this.parseServiceSchema({
            name: "api.slack.utilities",
            version: 4,
            actions: {
                markdownToMrkdwn: {
                    name: "markdown-to-mrkdwn",
                    params: {
                        text: "string"
                    },
                    handler: this.markdownToMrkdwn
                }
            }
        });
    }

    async markdownToMrkdwn(ctx: Context<{
        text: string
    }, any>) {
        const html = snuownd.getParser().render(ctx.params.text);
        return htmlToMrkdwn(html).text;
    }
}