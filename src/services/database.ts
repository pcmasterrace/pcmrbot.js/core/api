import { ServiceSchema } from "moleculer";

import initDb from "../models/index";

module.exports = {
    name: "api.database",
    version: 4,
    settings: {
        $secureSettings: ["db"],
        db: {
            connectionString: process.env.DB_CONNECTION_STRING
        }
    },
    created() {
        if (!this.settings.db.connectionString) this.broker.fatal("Database connection string not supplied! Make sure DB_CONNECTION_STRING is populated!");

        const gs = initDb(this.settings.db.connectionString);
        
        // This should configure a process-wide Sequelize object
        this.db = gs.db;
        this.models = gs.models;
    }
} as ServiceSchema;