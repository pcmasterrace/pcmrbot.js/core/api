import { Context, Errors, ServiceSchema } from "moleculer";
import Vault from "node-vault";
import delay from "delay";

module.exports = {
    name: "api.vault",
    version: 4,
    settings: {
        $secureSettings: ["vault"],
        vault: {
            apiVersion: process.env.API_VAULT_VERSION || "v1",
            endpoint: process.env.API_VAULT_ENDPOINT
        }
    },
    created() {
        this.vaultUnsealed = false;

        // Throw error if Vault endpoint is not supplied
        if (!this.settings.vault.endpoint) {
            this.broker.fatal("Vault logger endpoint not supplied! Make sure API_VAULT_ENDPOINT is populated!");
        }

        this.vault = Vault(this.settings.vault);
    },
    async started() {
        if (!(await this.vault.initialized()).initialized) {
            try {
                let results = await this.vault.init({secret_shares: 1, secret_threshold: 1});
                await this.vault.unseal({secret_shares: 1, key: results.keys[0]});
            } catch (err) {
                // In case of race condition with other services
                if (err.message !== "Vault is already initialized") {
                    throw err
                }
            }
        }

        while ((await this.vault.status()).sealed) {
            this.logger.info("Vault is sealed, sleeping for 1 second...");
            await delay(1000);
        }
    }
} as ServiceSchema